# Full path and name to your csv file
import os, sys
directorio = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

csv_filepathname= os.path.join(directorio, "HistoriasC/municipios.csv")
print csv_filepathname
# Full path to your django project directory
home= os.path.join(directorio, "HistoriasC/HistoriasC")

sys.path.append(home)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django
django.setup()

from Departamento.models import Departamento
from Municipio.models import Municipio


import csv
dataReader = csv.reader(open(csv_filepathname), delimiter=';', quotechar='"')
def utf8_encode(val):
    try:
        tmp = val.decode('utf8')
    except:
        try:
            tmp = val.decode('latin1')
        except:
            tmp= val.decode('ascii', 'ignore')
            tmp = tmp.encode('utf8')    
    return tmp
for row in dataReader:
    
    depto = Municipio()
    depto.COD_DEPTO = Departamento.objects.get(pk=row[0])
    depto.COD_MPIO = row[1]
    depto.NOM_MPIO = row[2]
    
    print "%s - %s - %s" % (depto.COD_DEPTO,depto.COD_MPIO,depto.NOM_MPIO)
    depto.save()