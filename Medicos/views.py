from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import *
# Create your views here.

def listarMedicos(request):
	medicos = Medico.objects.all()
	medicoCombo = []
	for medico in medicos:
		medicoDict = {}
		medicoDict["nombres"] = medico.nombres+" "+medico.apellidos
		medicoDict["id"] = medico.pk
		medicoCombo.append(medicoDict)
	return JsonResponse(medicoCombo, safe=False)

def listarHorarios(request):
	horarios = Horarios.objects.all()
	horarioCombo = []
	for horario in horarios:
		horarioDict = {}
		horarioDict["desc"] = horario.descripcion
		horarioDict["id"] = horario.pk
		horarioCombo.append(horarioDict)
	return JsonResponse(horarioCombo, safe=False)