# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Medico(models.Model):
	cuenta = models.OneToOneField(User)
	nombres = models.CharField(max_length=255)
	apellidos = models.CharField(max_length=255)
	tipo_documento = models.CharField(max_length=10)
	documento = models.CharField(max_length=255)
	registro_medico = models.CharField(max_length=200, verbose_name='Registro médico')

	profesion = models.CharField(max_length=255, blank=True, null=True)

	direccion = models.CharField(max_length=255, blank=True, null=True)
	telefonos = models.CharField(max_length=255, blank=True, null=True)
	correo_electronico = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		verbose_name='Médico'
		verbose_name_plural='Médicos'

	def __unicode__(self):
		return self.nombres

class Horarios(models.Model):
	descripcion = models.CharField(max_length=100)

	class Meta:
		verbose_name='Horario'
		verbose_name_plural='Horarios'
	def __unicode__(self):
		return self.descripcion