from django.contrib import admin
from .models import *
# Register your models here.

class MedicoAdmin(admin.ModelAdmin):
    list_display = ('nombres', 'apellidos', 'documento', 'profesion', 'telefonos',)

admin.site.register(Medico, MedicoAdmin)
admin.site.register(Horarios)