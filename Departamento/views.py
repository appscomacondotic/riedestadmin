from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.contenttypes.models import ContentType
from Departamento.models import Departamento
import json
# Create your views here.

@csrf_exempt
def listado_depto(request):
    # se hace la consuta de los registros
    depto = Departamento.objects.all()
    # construir una lista que contenga todos los datos de la respuesta
    mi_json = []
    for deptos in depto:
        # para cada objeto, construir un diccionario que contiene los datos que desea devolver
        depto_dict = {}
        depto_dict['id'] = deptos.COD_DEPTO
        depto_dict['nombre'] = deptos.NOM_DEPTO
        
        mi_json.append(depto_dict)
    # convertir la lista a JSON
    lista_respuesta = json.dumps(mi_json)
    # devolver un HttpResponse con el JSON 
    return HttpResponse(lista_respuesta)