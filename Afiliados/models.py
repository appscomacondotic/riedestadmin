# -*- coding: utf-8 -*-
from django.db import models
from Departamento.models import Departamento
from Municipio.models import Municipio
# Create your models here.

class Afiliado(models.Model):
	afiliadoid = models.CharField(max_length=255, blank=True, null=True)
	eps = models.CharField(max_length=255, blank=True, null=True)
	tipo_documento = models.CharField(max_length=255, blank=True, null=True)
	documento = models.CharField(max_length=255, blank=True, null=True)
	apellido1 = models.CharField(max_length=255, blank=True, null=True)
	apellido2 = models.CharField(max_length=255, blank=True, null=True)
	nombre1 = models.CharField(max_length=255, blank=True, null=True)
	nombre2 = models.CharField(max_length=255, blank=True, null=True)
	fecha_nac = models.CharField(max_length=255, blank=True, null=True)
	sexo = models.CharField(max_length=255, blank=True, null=True)
	tipo_afiliado = models.CharField(max_length=255, blank=True, null=True)
	grupo_poblacional = models.CharField(max_length=255, blank=True, null=True)
	parentesco = models.CharField(max_length=255, blank=True, null=True)
	nivel_sisben = models.CharField(max_length=255, blank=True, null=True)
	ficha_sisben = models.CharField(max_length=255, blank=True, null=True)
	condicion_beneficiario = models.CharField(max_length=255, blank=True, null=True)
	departamento = models.CharField(max_length=100, blank=True, null=True)
	municipio = models.CharField(max_length=100, blank=True, null=True)
	zona = models.CharField(max_length=255, blank=True, null=True)
	fecha_sistema = models.CharField(max_length=255, blank=True, null=True)
	fecha_entidad = models.CharField(max_length=255, blank=True, null=True)
	grupo_etnico = models.CharField(max_length=255, blank=True, null=True)
	modalidad_subsidio = models.CharField(max_length=255, blank=True, null=True)
	estado = models.CharField(max_length=255, blank=True, null=True)
	fecha_novedad = models.CharField(max_length=255, blank=True, null=True)
	periodo_aplicacion = models.CharField(max_length=255, blank=True, null=True)
	contrato = models.CharField(max_length=255, blank=True, null=True)
	valor_capitacion = models.CharField(max_length=255, blank=True, null=True)
	dias_capitacion = models.CharField(max_length=255, blank=True, null=True)
	tipo_ajuste_capitacion = models.CharField(max_length=255, blank=True, null=True)
	periodo_afectado = models.CharField(max_length=255, blank=True, null=True)
	periodo_liquidacion = models.CharField(max_length=255, blank=True, null=True)
	campo42 = models.CharField(max_length=255, blank=True, null=True)
	descripcion = models.CharField(max_length=255, blank=True, null=True)
	direccion = models.CharField(max_length=255, blank=True, null=True)
	barrio = models.CharField(max_length=255, blank=True, null=True)
	telefono = models.CharField(max_length=255, blank=True, null=True)

	def __unicode__(self):
		return self.apellido1+" "+self.apellido2+" "+self.nombre1+" "+self.nombre2

class ImportAfiliados(models.Model):
	afiliadosList = models.CharField(max_length=255, blank=True, null=True)
	class Meta:
		verbose_name='Importar afiliados'
		verbose_name_plural='Importar afiliados'

			