from django.contrib import admin
from .models import *
# Register your models here.

class AfiliadoAdmin(admin.ModelAdmin):
    list_display = ('documento','tipo_documento' ,'apellido1' ,'apellido2' ,'nombre1' ,'nombre2' ,'sexo' ,'departamento',)
    list_filter = ('departamento','tipo_documento')
    
    search_fields = ['documento']

admin.site.register(Afiliado, AfiliadoAdmin)