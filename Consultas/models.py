# -*- coding: utf-8 -*-
from django.db import models
from HistoriaClinica.models import HistoriaClinica
from Medicos.models import Medico, Horarios

# Create your models here.

class Consulta(models.Model):
	historia_clinica = models.ForeignKey(HistoriaClinica)
	fecha_consulta = models.DateTimeField()
	motivo = models.TextField(blank=True, null=True)
	medico = models.ForeignKey(Medico)
	fecha_creacion = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.historia_clinica.numero_historia


class Responsables(models.Model):
	consulta = models.ForeignKey(Consulta)
	nombre1 = models.CharField(max_length=255)
	nombre2 = models.CharField(max_length=255, blank=True, null=True)
	apellido1 = models.CharField(max_length=255)
	apellido2 = models.CharField(max_length=255, blank=True, null=True)
	ciudad = models.CharField(max_length=255, blank=True, null=True)
	direccion = models.TextField(blank=True, null=True)
	telefono = models.CharField(max_length=15, blank=True, null=True)
	parentesco = models.CharField(max_length=100, blank=True, null=True)

	class Meta:
		verbose_name = "Acompañante"
		verbose_name_plural = "Acompañantes"

	def __unicode__(self):
		return "%s %s" % (self.nombre1, self.apellido1)

class RevisionConsultaGeneral(models.Model):
	consulta = models.ForeignKey(Consulta)
	fecha_revision = models.DateField(auto_now_add=True)

	enfermedad_actual = models.TextField(blank=True, null=True)
	cardio_respiratorio = models.BooleanField(default=False, help_text="Positivo?")
	cardio_respiratorio_descripcion = models.CharField(max_length=255, blank=True, null=True)
	vascular = models.BooleanField(default=False, help_text="Positivo?")
	vascular_descripcion = models.CharField(max_length=255, blank=True, null=True)
	gastro_intestinal = models.BooleanField(default=False, help_text="Positivo?")
	gastro_intestinal_descripcion = models.CharField(max_length=255, blank=True, null=True)
	genito = models.BooleanField(default=False, help_text="Positivo?")
	genito_descripcion = models.CharField(max_length=255, blank=True, null=True)
	endocrino = models.BooleanField(default=False, help_text="Positivo?")
	endocrino_descripcion = models.CharField(max_length=255, blank=True, null=True)
	osteomuscular = models.BooleanField(default=False, help_text="Positivo?")
	osteomuscular_descripcion = models.CharField(max_length=255, blank=True, null=True)
	neurologo = models.BooleanField(default=False, help_text="Positivo?")
	neurologo_descripcion = models.CharField(max_length=255, blank=True, null=True)
	piel = models.BooleanField(default=False, help_text="Positivo?")
	piel_descripcion = models.CharField(max_length=255, blank=True, null=True)

	apariencia = models.TextField(blank=True, null=True)
	temperatura = models.DecimalField(max_digits = 40, decimal_places = 2, default=0, help_text='en ºC')
	talla = models.DecimalField(max_digits = 40, decimal_places = 2, default=0, help_text='en Mts')
	peso = models.DecimalField(max_digits = 40, decimal_places = 2, default=0, help_text='en Kgs')
	frecuencia_cardiaca = models.CharField(max_length=20, blank=True, null=True)
	frecuencia_respiratoria = models.CharField(max_length=20, blank=True, null=True)
	presion_arterial = models.CharField(max_length=20, help_text='en mmHg', blank=True, null=True)
	cabeza = models.BooleanField(default=False, help_text="Positivo?")
	cabeza_descripcion = models.CharField(max_length=255, blank=True, null=True)
	ojos = models.BooleanField(default=False, help_text="Positivo?")
	ojos_descripcion = models.CharField(max_length=255, blank=True, null=True)
	oidos = models.BooleanField(default=False, help_text="Positivo?")
	oidos_descripcion = models.CharField(max_length=255, blank=True, null=True)
	nariz = models.BooleanField(default=False, help_text="Positivo?")
	nariz_descripcion = models.CharField(max_length=255, blank=True, null=True)
	boca = models.BooleanField(default=False, help_text="Positivo?")
	boca_descripcion = models.CharField(max_length=255, blank=True, null=True)
	cuello = models.BooleanField(default=False, help_text="Positivo?")
	cuello_descripcion = models.CharField(max_length=255, blank=True, null=True)
	torax = models.BooleanField(default=False, help_text="Positivo?")
	torax_descripcion = models.CharField(max_length=255, blank=True, null=True)
	pulmones = models.BooleanField(default=False, help_text="Positivo?")
	pulmones_descripcion = models.CharField(max_length=255, blank=True, null=True)
	corazon = models.BooleanField(default=False, help_text="Positivo?")
	corazon_descripcion = models.CharField(max_length=255, blank=True, null=True)
	abdomen = models.BooleanField(default=False, help_text="Positivo?")
	abdomen_descripcion = models.CharField(max_length=255, blank=True, null=True)
	genitourinario = models.BooleanField(default=False, help_text="Positivo?")
	genitourinario_descripcion = models.CharField(max_length=255, blank=True, null=True)
	columna = models.BooleanField(default=False, help_text="Positivo?")
	columna_descripcion = models.CharField(max_length=255, blank=True, null=True)
	extremidades = models.BooleanField(default=False, help_text="Positivo?")
	extremidades_descripcion = models.CharField(max_length=255, blank=True, null=True)
	neurologico = models.BooleanField(default=False, help_text="Positivo?")
	neurologico_descripcion = models.CharField(max_length=255, blank=True, null=True)
	piel = models.BooleanField(default=False, help_text="Positivo?")
	piel_descripcion = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		verbose_name='Revisión General'
		verbose_name_plural='Revisión General'

	def __unicode__(self):
		return str(self.consulta.fecha_creacion)