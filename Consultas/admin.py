# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *
from Medicos.models import Medico
from Diagnosticos.models import Diagnostico, Medicacion
# Register your models here.

class RevisionGeneralInline(admin.StackedInline):
	model = RevisionConsultaGeneral
	suit_classes = "suit-tab suit-tab-revisionGeneral"
	extra = 1
	fieldsets = [
		(None, {
			'fields': [
				'cardio_respiratorio',
				'cardio_respiratorio_descripcion',
				'vascular',
				'vascular_descripcion',
				'gastro_intestinal',
				'gastro_intestinal_descripcion',
				'genito',
				'genito_descripcion',
				'endocrino',
				'endocrino_descripcion',
				'osteomuscular',
				'osteomuscular_descripcion',
				'neurologo',
				'neurologo_descripcion',
				'piel',
				'piel_descripcion',
			],
			'classes': ('suit-tab', 'suit-tab-revisionGeneral')
		}),
		("Examen físico", {
			'fields': [
				'apariencia',
				'temperatura',
				'talla',
				'peso',
				'frecuencia_cardiaca',
				'frecuencia_respiratoria',
				'presion_arterial',
				'cabeza',
				'cabeza_descripcion',
				'ojos',
				'ojos_descripcion',
				'oidos',
				'oidos_descripcion',
				'nariz',
				'nariz_descripcion',
				'boca',
				'boca_descripcion',
				'cuello',
				'cuello_descripcion',
				'torax',
				'torax_descripcion',
				'pulmones',
				'pulmones_descripcion',
				'corazon',
				'corazon_descripcion',
				'abdomen',
				'abdomen_descripcion',
				'genitourinario',
				'genitourinario_descripcion',
				'columna',
				'columna_descripcion',
				'extremidades',
				'extremidades_descripcion',
				'neurologico',
				'neurologico_descripcion',
				'piel',
				'piel_descripcion',
			],
			'classes': ('suit-tab', 'suit-tab-revisionGeneral')
		}),

	]

class DiagnosticoInline(admin.StackedInline):
	model = Diagnostico
	extra = 1
	filter_horizontal = ('codigo_cie10',)
	suit_classes = "suit-tab suit-tab-diagnosticos"

class MedicamentosInline(admin.StackedInline):
	model = Medicacion
	filter_horizontal = ('medicamentos',)
	max_num = 1
	suit_classes = "suit-tab suit-tab-medicacion"

class ResponsablesInline(admin.StackedInline):
	model = Responsables
	extra = 1
	suit_classes = "suit-tab suit-tab-responsables"

class ConsultaAdmin(admin.ModelAdmin):
	list_display = ('historia_clinica', 'fecha_consulta', 'medico',)
	list_filter = ('fecha_consulta', 'medico',)

	inlines = [RevisionGeneralInline, DiagnosticoInline, ResponsablesInline,]

	fieldsets = [
		(None, {
			'fields': [
				'historia_clinica',
				'fecha_consulta',
				'medico',	
			],
			'classes': ('suit-tab', 'suit-tab-general')
		}),
		("Motivo de consulta", {
			'fields': [
				'motivo',
			],
			'classes': ('suit-tab', 'suit-tab-general')
		})
	]


	suit_form_tabs = (('general', "General"), ('responsables', "Acompañantes"), ('revisionGeneral', "Revisión"), ('diagnosticos', "Diagnósticos"),)

	def save_model(self, request, obj, form, change):
		obj.save()


admin.site.register(Consulta, ConsultaAdmin)
admin.site.register(RevisionConsultaGeneral)
admin.site.register(Responsables)