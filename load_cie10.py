# Full path and name to your csv file
import os, sys
directorio = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

csv_filepathname= os.path.join(directorio, "HistoriasC/lista_cie10.csv")
print csv_filepathname
# Full path to your django project directory
home= os.path.join(directorio, "HistoriasC/HistoriasC")

sys.path.append(home)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django
django.setup()

from Diagnosticos.models import CodigoCie10


import csv
dataReader = csv.reader(open(csv_filepathname, 'rU'), delimiter=';', quotechar='"')
def utf8_encode(val):
    try:
        tmp = val.decode('utf8')
    except:
        try:
            tmp = val.decode('latin1')
        except:
            tmp= val.decode('ascii', 'ignore')
            tmp = tmp.encode('utf8')    
    return tmp
for row in dataReader:
    
    item = CodigoCie10()
    item.codigo = row[1]
    item.descripcion = row[2]
    
    print "%s - %s" % (item.codigo,item.descripcion)
    item.save()