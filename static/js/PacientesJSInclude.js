(function($) {
    $(document).ready(function() {

    	titulo = document.title;

        urlArr = document.URL.split('/');
        var formulario = '<form action="/admin/crearConsulta" id="formulario" method="get" style="margin-bottom: 0px;"><input type="hidden" name="pk" value="'+urlArr[urlArr.length - 2]+'"><input type="hidden" name="type" value="" /><button type="submit" class="btn btn-high btn-success">Consulta</button></form>';

        if(titulo.indexOf("Modificar") > -1){
        	$('.submit-row').prepend(formulario);


	        $('#formulario').submit(function(e){
	        	e.preventDefault();

	        	$.getJSON('/admin/traerHistorias/?paciente='+urlArr[urlArr.length - 2], function(json){
	        		$('#listaHistorias').empty();
	        		$.each(json, function(key, value){
	        			var id = "", numero = "", tipo = "", desc = "";
	        			$.each(value, function(k,v){
	        				if(k == "numero_historia")
	        					numero = v;

	        				if(k == "tipo_historia")
	        					tipo = v;

	        				if(k == "id")
	        					id = v;
	        				if(k == "descripcion_historia")
	        					desc = v;
	        			});
	        			$('#listaHistorias').append('<li><a href="/admin/crearConsulta?pk='+id+'&type='+tipo+'">'+numero+' - '+desc+'</a></li>');
	        		});
	        	});

	            $('#modalHistorias').modal('show');
	            return;
	        });
        }
        
    });
})(jQuery);