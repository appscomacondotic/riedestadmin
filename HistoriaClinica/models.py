# -*- coding: utf-8 -*-
from django.db import models
from Afiliados.models import Afiliado
# Create your models here.

class HistoriaClinica(models.Model):
	afiliado = models.OneToOneField(Afiliado)
	numero_historia = models.CharField(max_length=200, blank=True, null=True, unique=True)

	class Meta:
		verbose_name='Historia Clínica'
		verbose_name_plural='Historias Clínicas'

	def __unicode__(self):
		return self.numero_historia


class AntecedentesFamiliares(models.Model):
	historias = models.OneToOneField(HistoriaClinica)
	cardiopatias = models.BooleanField(default=False)
	diabetes = models.BooleanField(default=False)
	hipertension = models.BooleanField(default=False)
	asma = models.BooleanField(default=False)
	psiquiatrica = models.BooleanField(default=False)
	enfisema = models.BooleanField(default=False)
	cancer = models.BooleanField(default=False)
	epilepsia = models.BooleanField(default=False)
	hepatitis = models.BooleanField(default=False)
	otros = models.TextField(blank=True, null=True)
	class Meta:
		verbose_name = "Antecedente Familiar"
		verbose_name_plural = "Antecedentes Familiares"

	def __unicode__(self):
		return self.historias.afiliado.apellido1

class AntecedentesPersonales(models.Model):
	historias = models.OneToOneField(HistoriaClinica)
	patologico = models.TextField(blank=True, null=True)
	quirurgico = models.TextField(blank=True, null=True)
	traumatico = models.TextField(blank=True, null=True)
	alergicos = models.TextField(blank=True, null=True)
	ginecologico = models.TextField(blank=True, null=True)
	embarazo = models.CharField(max_length=255, blank=True, null=True)
	climaterio = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		verbose_name = "Antecedente Personal"
		verbose_name_plural = "Antecedentes Personales"

	def __unicode__(self):
		return self.historias.afiliado.apellido1

class Habitos(models.Model):
	historias = models.OneToOneField(HistoriaClinica)
	cigarrillo = models.BooleanField(default=False)
	alcohol = models.BooleanField(default=False)
	cafe = models.BooleanField(default=False)
	estres = models.BooleanField(default=False)
	ejercicio = models.BooleanField(default=False)

	#Dentales
	lengua_proctatil = models.BooleanField(default=False)
	canofagia = models.BooleanField(default=False)
	quelosfagia = models.BooleanField(default=False)
	respiracion_bucal = models.BooleanField(default=False)
	succion_digital = models.BooleanField(default=False)
	succion_labial = models.BooleanField(default=False)
	onicofagia = models.BooleanField(default=False)

	otros = models.TextField(blank=True, null=True)

	class Meta:
		verbose_name = "Habito"
		verbose_name_plural = "Habitos"

	def __unicode__(self):
		return self.historias.afiliado.apellido1