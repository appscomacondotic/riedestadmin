# -*- coding: utf-8 -*-
from django.contrib import admin
from Consultas.models import Consulta
from .models import *
# Register your models here.


class AntecedentesFamiliaresInline(admin.StackedInline):
	model = AntecedentesFamiliares
	extra = 1
	suit_classes = 'suit-tab suit-tab-antecedentesFamiliares'

class AntecedentesPersonalesInline(admin.StackedInline):
	model = AntecedentesPersonales
	extra = 1
	suit_classes = 'suit-tab suit-tab-antecedentesPersonales'

class HabitosInline(admin.StackedInline):
	model = Habitos
	extra = 1
	suit_classes = 'suit-tab suit-tab-habitos'	

class ConsultasInline(admin.TabularInline):
	model = Consulta
	extra = 1
	suit_classes = 'suit-tab suit-tab-consultas'
	fields = ('fecha_consulta', )
	readonly_fields = ('fecha_consulta', )

	def has_add_permission(self, request):
		return False

	def has_delete_permission(self, request, obj=None):
		return False

	def get_queryset(self, request):
		qs = super(ConsultasInline, self).get_queryset(request)
		qs = qs.order_by('-fecha_consulta')
		return qs

class HistoriasAdmin(admin.ModelAdmin):
	list_display = ('numero_historia', 'afiliado',)
	search_fields = ('numero_historia',)
	inlines = [AntecedentesFamiliaresInline, AntecedentesPersonalesInline, HabitosInline, ConsultasInline]
	suit_classes = 'suit-tab suit-tab-historia'
	suit_form_tabs = (('historia', "Historia clínica"), ('antecedentesFamiliares', "Antecedentes Familiares"), ('antecedentesPersonales', "Antecedentes Personales"), ('habitos', "Hábitos"),)



admin.site.register(HistoriaClinica, HistoriasAdmin)
admin.site.register(AntecedentesFamiliares)
admin.site.register(AntecedentesPersonales)
admin.site.register(Habitos)