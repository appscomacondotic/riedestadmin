from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.contenttypes.models import ContentType

import json
from Departamento.models import Departamento
from Municipio.models import Municipio

@csrf_exempt
def listar_mpio(request):
    areat = Departamento.objects.get(pk=request.POST.get('depto'))
    compo = Municipio.objects.filter(COD_DEPTO = areat)
    jsoncompo = []
    for compos in compo:
        compo_dict={}
        compo_dict['id']=compos.COD_MPIO
        compo_dict['nombre']=compos.NOM_MPIO
        jsoncompo.append(compo_dict)
    listar = json.dumps(jsoncompo)
    return HttpResponse(listar)
# Create your views here.

@csrf_exempt
def listar_municipios(request):
    areat = Departamento.objects.get(pk=request.POST.get('depto'))
    compo = Municipio.objects.filter(COD_DEPTO = areat)
    jsoncompo = []
    for compos in compo:
        compo_dict={}
        compo_dict['id']=compos.COD_MPIO
        compo_dict['nombre']=compos.NOM_MPIO
        jsoncompo.append(compo_dict)
    
    return HttpResponse(jsoncompo)