from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(CodigoCie10)
admin.site.register(Diagnostico)
admin.site.register(Medicamentos)
admin.site.register(Medicacion)