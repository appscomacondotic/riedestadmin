# -*- coding: utf-8 -*-
from django.db import models
from Consultas.models import Consulta
from Medicos.models import Medico
# Create your models here.

class CodigoCie10(models.Model):
	codigo = models.CharField(max_length=10)
	descripcion = models.CharField(max_length=255)
	class Meta:
		verbose_name='Código CIIE 10'
		verbose_name_plural='Lista de códigos CIIE 10'
	def __unicode__(self):
		return "%s - %s" % (self.codigo, self.descripcion)

class Diagnostico(models.Model):
	consulta = models.ForeignKey(Consulta)
	codigo_cie10 = models.ManyToManyField(CodigoCie10)
	observaciones = models.TextField(blank=True, null=True)

	def __unicode__(self):
		return self.observaciones

class Medicamentos(models.Model):
	codigo = models.CharField(max_length=100, unique=True)
	nombre = models.CharField(max_length=255)
	principio_activo = models.CharField(max_length=255, verbose_name='Principio Activo')
	presentacion = models.TextField(verbose_name='Presentación')
	edad_minima = models.IntegerField(verbose_name='Edad mínima de administración')
	edad_maxima = models.IntegerField(verbose_name='Edad máxima de administración')
	dosis_ninos = models.IntegerField(verbose_name='Dosis en niños')
	dosis_adultos = models.IntegerField(verbose_name='Dosis en adultos')
	dosis_maxima_ninos = models.IntegerField(verbose_name='Dosis máxima en niños')
	dosis_maxima_adultos = models.IntegerField(verbose_name='Dosis máxima en adultos')

	def __unicode__(self):
		return self.nombre

class Medicacion(models.Model):
	consulta = models.ForeignKey(Consulta)
	medico = models.ForeignKey(Medico)
	medicamentos = models.ManyToManyField(Medicamentos)

	def __unicode__(self):
		return self.medico.nombres