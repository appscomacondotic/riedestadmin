from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import *
from Consultas.models import Responsables
# Create your views here.

def viewMedicamento(request):
	return render(request, "formato-medicamentos.html", {})

def viewReferencia(request):
	return render(request, "formato-referencia.html", {})

@csrf_exempt
def getMedicamentos(request):
	medicamentos = Medicamentos.objects.all()
	comboMedicamentos = []
	for medicamento in medicamentos:
		medicDict = {}
		medicDict["pk"] = medicamento.codigo
		medicDict["nombre"] = medicamento.nombre
		comboMedicamentos.append(medicDict)
	return JsonResponse(comboMedicamentos, safe=False)

@csrf_exempt
def getMedicamento(request):
	try:
		medicamentos = Medicamentos.objects.get(codigo = request.POST["codigo"])
		comboMedicamentos = []
		medicDict = {}
		medicDict["pk"] = medicamentos.codigo
		medicDict["nombre"] = medicamentos.nombre
		medicDict["presentacion"] = medicamentos.presentacion
		medicDict["dosis"] = medicamentos.dosis_adultos
		medicDict["dosisMaxima"] = medicamentos.dosis_maxima_adultos
		comboMedicamentos.append(medicDict)

		return JsonResponse(comboMedicamentos, safe=False)
	except:
		return HttpResponse(0)





@csrf_exempt
def getCodigoCIE10Consulta(request):
	consulta = request.POST["consulta"]

	diagnosticos = Diagnostico.objects.filter(consulta__pk = consulta)

	diagnostico = ""
	if len(diagnosticos) > 0:
		diagnostico = diagnosticos[0].codigo_cie10.all()[0].codigo

	print diagnostico	
	return HttpResponse(diagnostico)

@csrf_exempt
def getAcompananteConsulta(request):
	consulta = request.POST["consulta"]

	responsables = Responsables.objects.filter(consulta__pk = consulta)
	if len(responsables) > 0:
		responsable = responsables[0]
		responsableCombo = []
		responsableDict = {}
		responsableDict["nombres"] = responsable.apellido1+ " "+responsable.apellido2+" "+responsable.nombre1+" "+responsable.nombre2
		responsableDict["telefono"] = responsable.telefono
		responsableDict["parentesco"] = responsable.parentesco
		responsableCombo.append(responsableDict)

		return JsonResponse(responsableCombo, safe=False)
	return HttpResponse(0)

@csrf_exempt
def saveMedicacion(request):
	pass

