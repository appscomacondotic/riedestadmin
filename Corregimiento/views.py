from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.contenttypes.models import ContentType
import json
from Corregimiento.models import Corregimiento
from Municipio.models import Municipio

@csrf_exempt
def listar_corre(request):
    areat = Municipio.objects.get(pk=request.POST.get('mpio'))
    compo = Corregimiento.objects.filter(COD_MPIO = areat)
    jsoncompo = []
    for compos in compo:
        compo_dict={}
        compo_dict['id']=compos.COD_CORRE
        compo_dict['nombre']=compos.NOM_CORRE
        jsoncompo.append(compo_dict)
    listar = json.dumps(jsoncompo)
    return HttpResponse(listar)
# Create your views here.
